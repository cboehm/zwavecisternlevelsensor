# zWaveCisternLevelSensor

# sources

Icon for cistern volume based on
https://www.veryicon.com/icons/system/icons8-metro-style/measurement-units-volume.html

Icon for cistern level
https://www.veryicon.com/icons/system/android-1/measurement-units-length-1.html

# hardware
- Z-Uno (https://z-uno.z-wave.me/)
- Ultraschallsensor JSN-SR04T 2.0 Ultrasonic Distance Sensor 
- Temperatursensor DS18B20
