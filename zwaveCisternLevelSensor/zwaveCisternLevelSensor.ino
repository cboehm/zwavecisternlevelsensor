// V7.2tem Test mit Parameter für Aktualisierungsintervall in Sekunden (77) und Aufhaenghoehe in mm (78)
// Code optimiert.
// Wie V6, Controller wird aber aktiv nur angesprochen, wenn sich ein Sensor ändert
// Ultraschallsensor JSN-SR04T 2.0 Ultrasonic Distance Sensor 
// Temperatursensor DS18B20
#include <Arduino.h>
#include <ZUNO_Definitions.h>
// Bibliothek fuer DS18B20
#include "ZUNO_DS18B20.h"

//Messintervall in Sekunden
//#define messIntervall 1
word messIntervall; //Messintervall als Parameter in Skunden
//#define messHoehe 2500 //Aufhänghöhe Sensor über Zisternenboden in mm 
word messHoehe; //Aufhänghöhe als Parameter 

//Konstanten & Variablen fuer JSN-SR04T 2.0 
#define trigPin 9      // PIN, die die Zeit-Messung anstösst, Start-Zeit, Schall senden (Trigger) 
#define echoPin 10     //PIN, über die die Zeitmessung beendet wir, Schall empfangen (Echo)
unsigned long Laufzeit=0;
word Fuellstand=0;
word Liter=0;

//Konstanten & Variablen fuer DS18B20
#define PIN_DS18B20 11 //PIN, an dem der Sensor angeschlossen ist
int Temperatur;
int AlteTemperatur;
byte addr1[8];

//Konstanten & Variablen fuer Berechnung Zisternenfuellung
//Annaeherungsformel zur Berechnung der Schallgeschwindigkeit 
//Schallgeschwindigkeit bei einer bestimmten Temperatur = 331,5 + (0,6 * Temperatur (in Celsius))
#define Schallgeschwindigkeit 331.5 //Schallgeschwindigkeit in m/s bei 0 Grad Celsius 
#define ZisternenDurchmesser 2000.0
#define pi 3.1415926

//Starten des 'Handlers' zum lesen der Z-Wave-Parameter
ZUNO_SETUP_CFGPARAMETER_HANDLER(zwave_parameter_hat_sich_geaendert);

// onewireVerbindung zum Temperatursensor 
OneWire ow(PIN_DS18B20);
DS18B20Sensor ds1820(&ow);

//Festlegen, welche Sensoren das Z-WAve-Gerät haben soll
ZUNO_SETUP_CHANNELS(
     ZUNO_SENSOR_MULTILEVEL(
         ZUNO_SENSOR_MULTILEVEL_TYPE_DISTANCE,
         SENSOR_MULTILEVEL_SCALE_METER,
         SENSOR_MULTILEVEL_SIZE_TWO_BYTES,
         SENSOR_MULTILEVEL_PRECISION_TWO_DECIMALS,
         BerechneterFuellstand
     ),
    ZUNO_SENSOR_MULTILEVEL(
         ZUNO_SENSOR_MULTILEVEL_TYPE_TANK_CAPACITY,
         SENSOR_MULTILEVEL_SCALE_LITER,
         SENSOR_MULTILEVEL_SIZE_TWO_BYTES,
         SENSOR_MULTILEVEL_PRECISION_ZERO_DECIMALS,
         BerechneteLiter
     ),
     ZUNO_SENSOR_MULTILEVEL(
         ZUNO_SENSOR_MULTILEVEL_TYPE_TEMPERATURE,
         SENSOR_MULTILEVEL_SCALE_CELSIUS,
         SENSOR_MULTILEVEL_SIZE_TWO_BYTES,
         SENSOR_MULTILEVEL_PRECISION_TWO_DECIMALS,
         GemesseneTemperatur
    )
);

//alles, was beim Starten des Gerätes durchlaufen wird
void setup() {
   // Define inputs and outputs Wasserstand
   pinMode(trigPin, OUTPUT);
   pinMode(echoPin, INPUT);

   // Adresse des Temperatursensors ermitteln
   ds1820.scanAloneSensor(addr1);

   //laden der Parameterwerte vom Z-Wave-Geraet
   messIntervall = zunoLoadCFGParam(77);
   messHoehe = zunoLoadCFGParam(78);
   AlteTemperatur = Temperatur;
     
   // Begin der seriellen Kommunikation mit vorgegebener Baudrate 9600
   //Serial.begin(9600);
   // Begin der seriellen Kommunikation, Versuch ohne vorgegebene Baudrate

   Serial.begin();
}

//alles, was regelmäßig durchlaufen wird
void loop() {

   //Temperatur messen

     // obtaining readings from the sensor ds18b20
     float tempF = ds1820.getTemperature(addr1);
     // make scaled word value for report
     Temperatur=int(tempF*100);
     //nur wirkliche Änderungen verarbeiten
     if (abs(AlteTemperatur - Temperatur) > 0.5) { 
       AlteTemperatur = Temperatur;
     }
     else {
       Temperatur = AlteTemperatur;
     }
     //Testausgabe Temperatur
     Serial.print("Temperatur: ");
     Serial.println(tempF);

   //Wasserstand ermitteln
     //Clear the trigPin by setting it LOW:
     digitalWrite(trigPin, LOW);
     delayMicroseconds(5);
     // Trigger the sensor by setting the trigPin high for 10 microseconds:
     digitalWrite(trigPin, HIGH);
     delayMicroseconds(20);
     digitalWrite(trigPin, LOW);

     // Read the echoPin. pulseIn() returns the Laufzeit (length of the pulse) in microseconds
     Laufzeit = pulseIn(echoPin, HIGH, 10000);

     // Berechnen des Wasserstandes
     // Fuellstand = Laufzeit / 2.0 * (Schallgeschwindigkeit + 0.6 * tempF)/1000;
     Fuellstand = messHoehe - Laufzeit / 2.0 * (Schallgeschwindigkeit + 0.6 * tempF)/1000.0;
     // Berechnen der Wassermenge
     Liter = pi / 4 *ZisternenDurchmesser*ZisternenDurchmesser*Fuellstand/1000000.0;

     // Test-Ausgabe der ermittelten Werte 
     Serial.print("Laufzeit = ");
     Serial.print(Laufzeit);
     Serial.print("ms, Schallgeschwindigkeit = ");
     Serial.print(Schallgeschwindigkeit + 0.6 * tempF);
     Serial.print("m/s, Abstand = ");
     Serial.print(messHoehe - Fuellstand);
     Serial.print("mm, Fuellstand = ");
     Serial.print(Fuellstand);
     Serial.print(" mm, Volumen = ");
     Serial.print(Liter);
     Serial.println(" Liter");
     Serial.print("Messinterval = ");
     Serial.print(messIntervall);
     Serial.println(" Sekunden");

// geaenderte Werte an Controler senden
     zunoSendReport(1);
     zunoSendReport(2);
     zunoSendReport(3);

     zunoSaveCFGParam(77, messIntervall);
     zunoSaveCFGParam(78, messHoehe);

   delay(messIntervall*1000);
}

//neu lesen der konfigurierten Parameter bei Aenderung, Aufruf durch "ZUNO_SETUP_CFGPARAMETER_HANDLER"
void zwave_parameter_hat_sich_geaendert(byte ParameterID, word ParameterWert) {
     if (ParameterID == 77) { 
       messIntervall = ParameterWert;
     }
     if (ParameterID == 78) { 
       messHoehe = ParameterWert;
     }
}

//Rückgabe-Funktionen für den Z-Uno, wird von zunoSendReport verwendet
word BerechneterFuellstand() {
     return Fuellstand/10;
}
word BerechneteLiter() {
     return Liter;
}
word GemesseneTemperatur() {
     return Temperatur;
}
